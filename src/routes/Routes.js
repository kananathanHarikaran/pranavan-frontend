import React, { lazy } from 'react';
import { Switch } from 'react-router-dom';
import Loader from './Loader';
import * as routes from 'constants/routes';
import AuthRoute from './AuthRoute';
import PrivateRoute from './PrivateRoute';

function Routes() {
  return (
    <Switch>
      <AuthRoute
        exact
        fallback={Loader}
        path={routes.LOGIN}
        component={lazy(() => import('pages/Auth/Login'))}
      />
      <PrivateRoute
        exact
        fallback={Loader}
        path={routes.HOME}
        component={lazy(() => import('pages/Home'))}
      />
      /** Start Master data related routes */
      <PrivateRoute
        exact
        model="floors"
        method="index"
        path={routes.FLOOR}
        component={lazy(() => import('pages/MasterData/Floor'))}
      />
      <PrivateRoute
        exact
        model="floors"
        method="create"
        path={routes.FLOOR_CREATE}
        component={lazy(() => import('pages/MasterData/Floor/Create'))}
      />
      <PrivateRoute
        exact
        model="floors"
        method="show"
        path={routes.FLOOR_SHOW}
        component={lazy(() => import('pages/MasterData/Floor/Show'))}
      />
      <PrivateRoute
        exact
        model="floors"
        method="edit"
        path={routes.FLOOR_EDIT}
        component={lazy(() => import('pages/MasterData/Floor/Edit'))}
      />
      <PrivateRoute
        exact
        model="designations"
        method="index"
        path={routes.DESIGNATION}
        component={lazy(() => import('pages/MasterData/Designation'))}
      />
      <PrivateRoute
        exact
        model="designations"
        method="create"
        path={routes.DESIGNATION_CREATE}
        component={lazy(() => import('pages/MasterData/Designation/Create'))}
      />
      <PrivateRoute
        exact
        model="designations"
        method="show"
        path={routes.DESIGNATION_SHOW}
        component={lazy(() => import('pages/MasterData/Designation/Show'))}
      />
      <PrivateRoute
        exact
        model="designations"
        method="edit"
        path={routes.DESIGNATION_EDIT}
        component={lazy(() => import('pages/MasterData/Designation/Edit'))}
      />
      <PrivateRoute
        exact
        model="Room Type"
        method="index"
        path={routes.ROOM_TYPE}
        component={lazy(() => import('pages/MasterData/RoomTypes'))}
      />
      <PrivateRoute
        exact
        model="Room Type"
        method="create"
        path={routes.ROOM_TYPE_CREATE}
        component={lazy(() => import('pages/MasterData/RoomTypes/Create'))}
      />
      <PrivateRoute
        exact
        model="Room Type"
        method="show"
        path={routes.ROOM_TYPE_SHOW}
        component={lazy(() => import('pages/MasterData/RoomTypes/Show'))}
      />
      <PrivateRoute
        exact
        model="Room Type"
        method="edit"
        path={routes.ROOM_TYPE_EDIT}
        component={lazy(() => import('pages/MasterData/RoomTypes/Edit'))}
      />
      /** End Master data related routes */ /** Start Setting related routes */
      <PrivateRoute
        exact
        model="Room Type"
        method="index"
        path={routes.HOTEL}
        component={lazy(() => import('pages/Settings/Hotel'))}
      />
      <PrivateRoute
        exact
        model="Hotel"
        method="create"
        path={routes.HOTEL_CREATE}
        component={lazy(() => import('pages/Settings/Hotel/Create'))}
      />
      <PrivateRoute
        exact
        model="Hotel"
        method="edit"
        path={routes.HOTEL_EDIT}
        component={lazy(() => import('pages/Settings/Hotel/Edit'))}
      />
      <PrivateRoute
        exact
        model="Hotel"
        method="show"
        path={routes.HOTEL_SHOW}
        component={lazy(() => import('pages/Settings/Hotel/Show'))}
      />
      <PrivateRoute
        exact
        model="Customer"
        method="index"
        path={routes.CISTOMER}
        component={lazy(() => import('pages/Customer'))}
      />
      <PrivateRoute
        exact
        model="Customer"
        method="create"
        path={routes.CISTOMER_CREATE}
        component={lazy(() => import('pages/Customer/Create'))}
      />
      <PrivateRoute
        exact
        model="Customer"
        method="edit"
        path={routes.CISTOMER_EDIT}
        component={lazy(() => import('pages/Customer/Edit'))}
      />
      <PrivateRoute
        exact
        model="Customer"
        method="show"
        path={routes.CISTOMER_SHOW}
        component={lazy(() => import('pages/Customer/Show'))}
      />
      /** End Setting related routes */
      <PrivateRoute
        exact
        model="Item"
        method="index"
        path={routes.ITEMS}
        component={lazy(() => import('pages/bar/Item'))}
      />
    </Switch>
  );
}

export default Routes;
