export const HOME = '/';
export const LOGIN = '/login';

export const FLOOR = '/master-datas/floors';
export const FLOOR_CREATE = '/master-datas/floors-create';
export const FLOOR_SHOW = '/master-datas/floors/:id';
export const FLOOR_EDIT = '/master-datas/floors/:id/edit';

export const DESIGNATION = '/master-datas/designations';
export const DESIGNATION_CREATE = '/master-datas/designations-create';
export const DESIGNATION_SHOW = '/master-datas/designations/:id';
export const DESIGNATION_EDIT = '/master-datas/designations:id/edit';

export const ROOM_TYPE = '/master-datas/room-types';
export const ROOM_TYPE_CREATE = '/master-datas/room-types-create';
export const ROOM_TYPE_SHOW = '/master-datas/room-types/:id';
export const ROOM_TYPE_EDIT = '/master-datas/room-types:id/edit';

export const HOTEL = '/settings/hotels';
export const HOTEL_CREATE = '/settings/hotels-create';
export const HOTEL_SHOW = '/settings/hotels/:id';
export const HOTEL_EDIT = '/settings/hotels/:id/edit';

export const CISTOMER = '/customers';
export const CISTOMER_CREATE = '/customers-create';
export const CISTOMER_SHOW = '/customers/:id';
export const CISTOMER_EDIT = '/customers/:id/edit';

export const ITEMS = '/items';
export const ITEMS_CREATE = '/items-create';
export const ITEMS_SHOW = '/items/:id';
export const ITEMS_EDIT = '/items/:id/edit';
