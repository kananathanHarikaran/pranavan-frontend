/** Floor Related message */
export const FLOOR_CREATE_SUCCESS = 'Floor create succeed!';
export const FLOOR_UPDATE_SUCCESS = 'Floor updated succeed!';

/** Designation Related message */
export const DESIGNATION_CREATE_SUCCESS = 'Designation create succeed!';
export const DESIGNATION_UPDATE_SUCCESS = 'Designation updated succeed!';

/** Room type Related message */
export const ROOM_TYPE_CREATE_SUCCESS = 'Room type create succeed!';
export const ROOM_TYPE_UPDATE_SUCCESS = 'Room type updated succeed!';


/** Customer  Related message */
export const CUSTOMER_CREATE_SUCCESS = 'Customer create succeed!';
export const CUSTOMER_UPDATE_SUCCESS = 'Customer updated succeed!';


/** Hotel  Related message */
export const HOTEL_CREATE_SUCCESS = 'Hotel create succeed!';
export const HOTEL_UPDATE_SUCCESS = 'Hotel updated succeed!';
