import React from 'react';
import { withFormik } from 'formik';
import { object, string } from 'yup';
import { Stack } from '@fluentui/react/lib/Stack';
import { TextField } from '@fluentui/react/lib/TextField';
import { PrimaryButton, DefaultButton } from '@fluentui/react/lib/Button';
import * as routes from 'constants/routes';

function Form({
  isValid,
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleFocus,
  handleSubmit,
  isSubmitting,
  buttonText,
  history,
}) {
  return (
    <form onSubmit={handleSubmit}>
      <Stack tokens={{ childrenGap: 15 }}>
        <TextField
          name="full_name"
          label="Full Name"
          value={values.full_name}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter full name here"
          errorMessage={touched.full_name && errors.full_name}
        />
        <TextField
          name="nic_number"
          label="Nic Number"
          value={values.nic_number}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter nic here"
          errorMessage={touched.nic_number && errors.nic_number}
        />
        <TextField
          name="passport_number"
          label="Passport Number"
          value={values.passport_number}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter passport here"
          errorMessage={touched.passport_number && errors.passport_number}
        />
        <TextField
          name="phone"
          label="Phone Number"
          value={values.phone}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter phone here"
          errorMessage={touched.phone && errors.phone}
        />
        <TextField
          name="email"
          label="Email"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter email here"
          errorMessage={touched.email && errors.email}
        />
        <TextField
          name="company"
          label="Company"
          value={values.company}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter company here"
          errorMessage={touched.company && errors.company}
        />
        <TextField
          name="address"
          label="Address"
          value={values.address}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter address here"
          errorMessage={touched.address && errors.address}
        />

        <Stack.Item align="end">
          <Stack horizontal tokens={{ childrenGap: 10 }}>
            <PrimaryButton
              type="submit"
              onClick={handleSubmit}
              disabled={!isValid || isSubmitting}
              styles={{ root: { marginBottom: '10px' } }}
              text={isSubmitting ? 'Submitting...' : buttonText}
            />
            <DefaultButton
              text="Cancel"
              onClick={() => history.push(routes.CISTOMER)}
              disabled={isSubmitting}
            />
          </Stack>
        </Stack.Item>
      </Stack>
    </form>
  );
}

export default withFormik({
  validationSchema: object().shape({
    full_name: string().required('Name is a required field'),
    nic_number: string().required('Nic is a required field'),
    passport_number: string().required('Passport is a required field'),
    phone: string().required('Phone is a required field'),
    email: string().email().required('Email is a required field'),
    company: string().nullable(),
    address: string().required('Address is a required field'),
  }),

  mapPropsToValues: ({ item }) => ({
    full_name: item?.full_name,
    nic_number: item?.nic_number,
    passport_number: item?.passport_number,
    phone: item?.phone,
    email: item?.email,
    address: item?.address,
    company: item?.company,
  }),
  handleSubmit: (values, { props, ...actions }) => {
    props.onSubmit(values, actions);
  },
})(Form);
