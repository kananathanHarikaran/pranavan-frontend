import React from 'react';
import { withFormik } from 'formik';
import { object, string } from 'yup';
import { Stack } from '@fluentui/react/lib/Stack';
import { TextField } from '@fluentui/react/lib/TextField';
import { PrimaryButton, DefaultButton } from '@fluentui/react/lib/Button';
import * as routes from 'constants/routes';

function Form({
  isValid,
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleFocus,
  handleSubmit,
  isSubmitting,
  buttonText,
  setFieldValue,
  history,
}) {
  return (
    <form onSubmit={handleSubmit}>
      <Stack tokens={{ childrenGap: 15 }}>
        <TextField
          name="name"
          label="Name"
          value={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter name here"
          errorMessage={touched.name && errors.name}
        />
        <TextField
          name="street_address"
          label="Street Address"
          value={values.street_address}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter street here"
          errorMessage={touched.name && errors.street_address}
        />
        <TextField
          name="city"
          label="city"
          value={values.city}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter city here"
          errorMessage={touched.name && errors.city}
        />
        <TextField
          name="zip_code"
          label="Zip code"
          value={values.zip_code}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter zip code here"
          errorMessage={touched.name && errors.zip_code}
        />

        <TextField
          name="country"
          label="Country"
          value={values.country}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter country  here"
          errorMessage={touched.name && errors.country}
        />

        <Stack.Item align="end">
          <Stack horizontal tokens={{ childrenGap: 10 }}>
            <PrimaryButton
              type="submit"
              onClick={handleSubmit}
              disabled={!isValid || isSubmitting}
              styles={{ root: { marginBottom: '10px' } }}
              text={isSubmitting ? 'Submitting...' : buttonText}
            />
            <DefaultButton
              text="Cancel"
              onClick={() => history.push(routes.CISTOMER)}
              disabled={isSubmitting}
            />
          </Stack>
        </Stack.Item>
      </Stack>
    </form>
  );
}

export default withFormik({
  validationSchema: object().shape({
    name: string().required('Name is a required field'),
    street_address: string().required('Address is a required field'),
    city: string().required('City is a required field'),
    country: string().required('Country is a required field'),
    zip_code: string().required('Zipcode is a required field'),
  }),

  mapPropsToValues: ({ item }) => ({
    name: item?.name,
    street_address: item?.street_address,
    country: item?.country,
    city: item?.city,
    zip_code: item?.zip_code,
  }),
  handleSubmit: (values, { props, ...actions }) => {
    props.onSubmit(values, actions);
  },
})(Form);
