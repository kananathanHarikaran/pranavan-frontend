import React from 'react';
import { Stack } from '@fluentui/react/lib/Stack';
import Breadcrumb from 'components/Breadcrumb';
import ResourceTable from 'components/ResourceTable';
import { SearchBox } from '@fluentui/react/lib/SearchBox';
import { CommandButton } from '@fluentui/react';
import { searchRoute } from 'lib/helpers';
import * as routes from 'constants/routes';

const breadcrumbItems = [
  {
    text: 'Settings',
    key: 'settings',
  },
  {
    text: 'Hotel',
    key: 'index',
    isCurrentItem: true,
  },
];

export const columns = [
  {
    key: 'id',
    name: 'ID',
    fieldName: 'id',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    minWidth: 50,
    maxWidth: 50,
    data: 'number',
    isPadded: true,
  },
  {
    key: 'reference_number',
    name: 'Reference Number',
    fieldName: 'reference_number',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'name',
    name: 'Name',
    fieldName: 'name',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'country',
    name: 'Country',
    fieldName: 'country',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'city',
    name: 'City',
    fieldName: 'city',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'zip_code',
    name: 'ZIP Code',
    fieldName: 'zip_code',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'street_address',
    name: 'Address',
    fieldName: 'street_address',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
];

function Customer({ history, location }) {
  return (
    <Stack className="inner-page-panel">
      <Breadcrumb items={breadcrumbItems} />
      <Stack
        horizontal
        verticalAlign="center"
        tokens={{ padding: '5px 10px 5px 0' }}
        horizontalAlign="space-between">
        <CommandButton text="Create" iconProps={{ iconName: 'Add' }} onClick={() => history.push(routes.HOTEL_CREATE)} />
        <SearchBox
          styles={{
            root: { width: 400, float: 'right', paddingRight: '5px' },
          }}
          placeholder="Search"
          onEscape={() => history.push(searchRoute(location, { query: '' }))}
          onClear={() => history.push(searchRoute(location, { query: '' }))}
          onSearch={(query) => history.push(searchRoute(location, { query }))}
        />
      </Stack>
      <Stack
        horizontal
        verticalAlign="center"
        tokens={{ padding: '5px 10px 5px 0' }}
        horizontalAlign="space-between"></Stack>
      <ResourceTable url="hotels" columns={columns} editRoute={(item) => routes.HOTEL_EDIT.replace(':id', item.id)}
        viewRoute={(item) => routes.HOTEL_SHOW.replace(':id', item.id)}
        deleteUrl={(item) => `hotels/${item.id}`} />
    </Stack>
  );
}

export default Customer;
