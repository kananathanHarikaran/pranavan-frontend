import React from 'react';
import { Stack } from '@fluentui/react/lib/Stack';
import { SearchBox } from '@fluentui/react/lib/SearchBox';
import { CommandButton } from '@fluentui/react';
import { searchRoute } from 'lib/helpers';
import Breadcrumb from 'components/Breadcrumb';
import ResourceTable from 'components/ResourceTable';
import * as routes from 'constants/routes';

const breadcrumbItems = [
  {
    text: 'Master Data',
    key: 'Master-data',
  },
  {
    text: 'Floor',
    key: 'index',
    isCurrentItem: true,
  },
];

export const columns = [
  {
    key: 'id',
    name: 'ID',
    fieldName: 'id',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    minWidth: 50,
    maxWidth: 50,
    data: 'number',
    isPadded: true,
  },
  {
    key: 'name',
    name: 'Name',
    fieldName: 'name',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
];

function Customer({ history, location }) {
  return (
    <Stack className="inner-page-panel">
      <Breadcrumb items={breadcrumbItems} />
      <Stack
        horizontal
        verticalAlign="center"
        tokens={{ padding: '5px 10px 5px 0' }}
        horizontalAlign="space-between">
        <CommandButton
          text="Create"
          iconProps={{ iconName: 'Add' }}
          onClick={() => history.push(routes.FLOOR_CREATE)}
        />
        <SearchBox
          styles={{
            root: { width: 400, float: 'right', paddingRight: '5px' },
          }}
          placeholder="Search"
          onEscape={() => history.push(searchRoute(location, { query: '' }))}
          onClear={() => history.push(searchRoute(location, { query: '' }))}
          onSearch={(query) => history.push(searchRoute(location, { query }))}
        />
      </Stack>
      <Stack
        horizontal
        verticalAlign="center"
        tokens={{ padding: '5px 10px 5px 0' }}
        horizontalAlign="space-between"></Stack>
      <ResourceTable
        url="floors"
        columns={columns}
        viewRoute={(item) => routes.FLOOR_SHOW.replace(':id', item.id)}
        deleteUrl={(item) => `floors/${item.id}`}
        editRoute={(item) => routes.FLOOR_EDIT.replace(':id', item.id)}
      />
    </Stack>
  );
}

export default Customer;
