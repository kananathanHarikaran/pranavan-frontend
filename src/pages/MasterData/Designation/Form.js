import React from 'react';
import { withFormik } from 'formik';
import { object, string } from 'yup';
import { Stack } from '@fluentui/react/lib/Stack';
import { TextField } from '@fluentui/react/lib/TextField';
import { PrimaryButton, DefaultButton } from '@fluentui/react/lib/Button';
import * as routes from 'constants/routes';

function Form({
  isValid,
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleFocus,
  handleSubmit,
  isSubmitting,
  buttonText,
  setFieldValue,
  history,
}) {
  return (
    <form onSubmit={handleSubmit}>
      <Stack tokens={{ childrenGap: 15 }}>
        <TextField
          name="name"
          label="Name"
          value={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          disabled={isSubmitting}
          placeholder="Enter name here"
          errorMessage={touched.name && errors.name}
        />

        <Stack.Item align="end">
          <Stack horizontal tokens={{ childrenGap: 10 }}>
            <PrimaryButton
              type="submit"
              onClick={handleSubmit}
              disabled={!isValid || isSubmitting}
              styles={{ root: { marginBottom: '10px' } }}
              text={isSubmitting ? 'Submitting...' : buttonText}
            />
            <DefaultButton
              text="Cancel"
              onClick={() => history.push(routes.DESIGNATION)}
              disabled={isSubmitting}
            />
          </Stack>
        </Stack.Item>
      </Stack>
    </form>
  );
}

export default withFormik({
  validationSchema: object().shape({
    name: string().required('Name is a required field'),
  }),
  mapPropsToValues: ({ item }) => ({
    name: item?.name,
  }),
  handleSubmit: (values, { props, ...actions }) => {
    props.onSubmit(values, actions);
  },
})(Form);
