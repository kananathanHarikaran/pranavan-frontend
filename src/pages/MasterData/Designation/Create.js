import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { Stack } from '@fluentui/react/lib/Stack';
import Breadcrumb from 'components/Breadcrumb';
import { storeItem } from 'actions/masterData/Designation';
import * as routes from 'constants/routes';
import Form from './Form';
import toast from 'lib/toast';
import { DESIGNATION_CREATE_SUCCESS } from 'constants/messages';

function Create({ history }) {
  const dispatch = useDispatch();

  const breadcrumbItems = [
    {
      text: 'Designation',
      key: 'index',
      onClick: () => history.push(routes.DESIGNATION),
    },
    {
      text: 'Create',
      key: 'create',
      isCurrentItem: true,
    },
  ];

  const onStore = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      try {
        await dispatch(storeItem(values));
        toast.success(DESIGNATION_CREATE_SUCCESS);
        history.push(routes.DESIGNATION);
      } catch (e) {
        if (e.errors) {
          actions.setErrors(e.errors);
        }
        actions.setSubmitting(false);
      }
    },
    [dispatch, history],
  );

  return (
    <Stack className="inner-page-panel">
      <Breadcrumb items={breadcrumbItems} />
      <Stack tokens={{ padding: 15 }}>
        <Stack className="form-panel" tokens={{ padding: 15 }}>
          <Form buttonText="Create" onSubmit={onStore} history={history} />
        </Stack>
      </Stack>
    </Stack>
  );
}

export default Create;
