import React from 'react';
import { Stack } from '@fluentui/react/lib/Stack';
import Breadcrumb from 'components/Breadcrumb';
import ResourceTable from 'components/ResourceTable';
import { SearchBox } from '@fluentui/react/lib/SearchBox';
import { CommandButton } from '@fluentui/react';
import { searchRoute } from 'lib/helpers';

const breadcrumbItems = [
  {
    text: 'BAR',
    key: 'bar',
  },
  {
    text: 'Items',
    key: 'index',
    isCurrentItem: true,
  },
];

export const columns = [
  {
    key: 'id',
    name: 'ID',
    fieldName: 'id',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    minWidth: 50,
    maxWidth: 50,
    data: 'number',
    isPadded: true,
  },
  {
    key: 'name',
    name: 'Name',
    fieldName: 'name',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'code',
    name: 'Code',
    fieldName: 'code',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'available_stock',
    name: 'Available',
    fieldName: 'available_stock',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'damage',
    name: 'Damage',
    fieldName: 'damage',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'reorder_level',
    name: 'Reorder Level',
    fieldName: 'reorder_level',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'purchasing_price',
    name: 'Purchasing Price',
    fieldName: 'purchasing_price',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
  {
    key: 'selling_price',
    name: 'Selling Price',
    fieldName: 'selling_price',
    isRowHeader: true,
    isResizable: true,
    isSorted: false,
    data: 'string',
    isPadded: true,
  },
];

function Item({ history, location }) {
  return (
    <Stack className="inner-page-panel">
      <Breadcrumb items={breadcrumbItems} />
      <Stack
        horizontal
        verticalAlign="center"
        tokens={{ padding: '5px 10px 5px 0' }}
        horizontalAlign="space-between">
        <CommandButton text="Create" iconProps={{ iconName: 'Add' }} />
        <SearchBox
          styles={{
            root: { width: 400, float: 'right', paddingRight: '5px' },
          }}
          placeholder="Search"
          onEscape={() => history.push(searchRoute(location, { query: '' }))}
          onClear={() => history.push(searchRoute(location, { query: '' }))}
          onSearch={(query) => history.push(searchRoute(location, { query }))}
        />
      </Stack>
      <Stack
        horizontal
        verticalAlign="center"
        tokens={{ padding: '5px 10px 5px 0' }}
        horizontalAlign="space-between"></Stack>
      <ResourceTable url="items" columns={columns} disableAction />
    </Stack>
  );
}

export default Item;
